package org.gtr.web;

import java.sql.Connection;
import java.sql.DriverManager;

import org.junit.Test;

public class MySQLConnectionTest {
	
	private static final String DRIVER =
			"com.mysql.jdbc.Driver";
	private static final String URL =
			"jdbc:mysql://127.0.0.1:3306/gtr";
	private static final String USER =
			"gtr";
	private static final String PW =
			"gtr12#$";

@Test
public void testConnection() throws Exception{
	Class.forName(DRIVER);
	Class.forName("net.sf.log4jdbc.sql.jdbcapi.DriverSpy");
	
	try(Connection con = DriverManager.getConnection(URL, USER, PW)){
		System.out.println(con);
	}catch(Exception e){
		e.printStackTrace();
	}
}
}