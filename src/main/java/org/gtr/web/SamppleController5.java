package org.gtr.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.gtr.domain.ProductVO;

@Controller
public class SamppleController5 {
	
	@RequestMapping(value="doJSON", produces="application/json")
	public @ResponseBody ProductVO doJSON() {
		ProductVO vo = new ProductVO("���û�ǰ",30000);
		
		return vo;
	}

}
